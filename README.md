# Reverberation

This repository provides an octave script ('demo_room.m') that
demonstrate how to simulate the reverberation of a room by convolution
with its "impulse response"

## Data source

The impulse response has been published as

   Stewart, Rebecca and Sandler, Mark. "Database of Omnidirectional
   and B-Format Impulse Responses", in Proc. of IEEE Int. Conf. on
   Acoustics, Speech, and Signal Processing (ICASSP 2010), Dallas,
   Texas, March 2010. 

and available at

   http://isophonics.net/content/room-impulse-response-data-set
