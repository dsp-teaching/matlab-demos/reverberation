
[signal, fs]=audioread('snark.flac');

distance = 3; % in m
round_trip_time = 2*distance / 330;
ritardo = round(round_trip_time * fs);

alfa = 0.7;

den = [1 zeros(1,ritardo) alfa];
num = fliplr(den);

% figure(1)
% 
% freqz(1, den);
% 
% figure(2)
% 
% freqz(num, den, 8*ritardo);

solo_poli=filter(1, den, signal);
passa_tutto=filter(num, den, signal);

soundsc(signal, fs)

soundsc(solo_poli, fs);

soundsc(signal, fs)

soundsc(passa_tutto, fs);



