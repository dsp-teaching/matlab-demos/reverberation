 %
 % This script octave demonstrates how to simulate a room 
 % reverberation by using the impulse response of the room.
 %

 %
 % Load the impulse response of the room.  This sample has been taken
 % from
 %
 %   Stewart, Rebecca and Sandler, Mark. "Database of
 %   Omnidirectional and B-Format Impulse Responses", in
 %   Proc. of IEEE Int. Conf. on Acoustics, Speech, and Signal
 %   Processing (ICASSP 2010), Dallas, Texas, March 2010.
 %
 % See
 %
 %   http://isophonics.net/content/room-impulse-response-data-set
 %
 %
 % Actually, the impulse responses are sampled at 96 kHz, while
 % the test signal is sampled at 22kHz.  The original impulse response
 % has been decimated by a factor of 4, bringing it to 24 kHz.
 % The two sampling frequency are not exactly equal, but for
 % demo purposes they are good enough (and I had no "sbatti" to do
 % something finer)
 %

[room, fs] = audioread('room.flac');

 %
 % Read the test signal
 %

 [music, fs2] = audioread('la-espero.flac');

 if fs != fs2
   error('Sampling frequency mismatch')
 end
 
 %
 % Now do the reverberation  (yes, it is just a conv)
 %

 disp('Computing convolution (direct)')
 
 tic
 with_reverber = conv(room, music);
 T_direct = toc;

 %
 % Compute the convolution using the FFT
 %

 disp('Computing convolution (FFT)')
 
 tic

 %
 % Add enough zeros to make room for the result
 %
 room_padded =[room(:); zeros(length(music), 1)];
 music_padded = [music(:); zeros(length(room), 1)];

 %
 % Product of the two DFT
 %
 R = fft(room_padded);
 M = fft(music_padded);
 C = R .* M;

 %
 % Inverse FFT
 %
 with_reverb_fast = real(ifft(C));

 T_fft=toc;

 %
 printf('Time direct = %g s\n', T_direct);
 printf('Time FFT    = %g s\n', T_fft);

 disp('Playing original');
 soundsc(music, fs);
 
 disp('Playing reverber (direct)');
 soundsc(with_reverber, fs);
 
 disp('Playing reverber (FFT)');
 soundsc(with_reverb_fast, fs);
 
 

 
